import jsonWebToken from 'jsonwebtoken';
import User from "../models/User";
import UserController from "../controllers/UsersController";
import router from "../routes/routes";

class Auth{
    static auth(roles){
        return async (req, res , next) => {
            try {
                let token = req.headers.authorization.replace(/Bearer/g, '');
                let decryptToken = jsonWebToken.decode(token, 'monsecret');
                let user = await User.findById(decryptToken.sub);

                if(user && user.user_role == 10){
                    //next est une fonction prédéfinie par express, elle indique que l'on peut passer a la fonction suivante dans la route,
                    //router.get('/users', Auth.auth([1, 10]), UserController.list);  <== ici on bascule sur UserController.list
                    next();
                }else if(user && roles.includes(user.user_role)){
                    next();
                }else{
                    res.status(401).json({'message:':'Error'})
                }
            }catch(error) {

            }
        }
    }
}

export default Auth;