import Comment from "../models/Comment";

import Post from "../models/Post";


class CommentsController{
    static async create(req,res){
        let status = 200;
        let body = {};

        try {
            let comment = await Comment.create({
                content: req.body.content,
                userId: req.body.userId,
                articleId: req.body.articleId
            });
            body = {
                'message': `Post ${post.title} created`,
                comment
            }
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }
        return res.status(status).json(body);
    }

    static async list(req, res) {
        let status = 200;
        let body = {};

        try {
            let comment = await Comment.find().populate('userId','postId');

            body = {
                comment,
                'message': 'Comment list'
            };
        } catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async details(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let comment = await Comment.findById(id);

            body = {
                'message': `Detail from ${comment.content}`,
                comment
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async update(req, res){
        let status = 200;
        let body = {};

        try {
            // Methode 3
            await Comment.findByIdAndUpdate(
                req.params.id,
                req.body,
                {new: true}
            );

            body = {
                'message': `200 \n\t comment updated`,
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }

    static async delete(req, res){
        let status = 200;
        let body = {};

        try {
            let id = req.params.id;
            let post = await Post.findByIdAndRemove(id);

            //await User.remove({_id: req.params.id});
            body = {
                'message': `comment deleted`
            };
        }
        catch (error) {
            status = 500;
            body = {'message': error.message};
        }

        return res.status(status).json(body);
    }
}

export default CommentsController;