// on indique {} dans l'import pour signifiez que l'on souhaite une sous classe de la classe express, sans les {} , cela voudrait dire qu'on veut une classe Router dans express ce qui serait incorrect
import { Router } from 'express';
import UserController from "../controllers/UsersController";
import ArticlesController from "../controllers/ArticlesController";
import PostsController from "../controllers/PostsController";
import CommentsController from "../controllers/CommentsController";
import Auth from "../config/auth";

const router = Router();

// ROUTER
// GET, POST, PUT, DELETE, PATCH
router.get('/test', function (req, res) {
    res.send("test");
});
// router.get('/users', function (req, res) {
//     res.send(UserController.list(req,res));
// });


//User routes
router.get('/users', UserController.list);
// router.get('/users', Auth.auth([1, 10]), UserController.list);
//router.get('/users', UserController.list);
router.post('/users/authentificate', UserController.auth)
router.post('/users', UserController.create);
router.get('/users/:id', UserController.details);
router.delete('/users/:id', UserController.delete);
router.put('/users/:id', UserController.update);

// articles routes
router.get('/articles', ArticlesController.list);
router.post('/articles', ArticlesController.create);
router.get('/articles/:id', ArticlesController.details);
router.delete('/articles/:id', ArticlesController.delete);
router.put('/articles/:id', ArticlesController.update);

// post routes
router.get('/posts', PostsController.list);
router.post('/posts', PostsController.create);
router.get('/posts/:id', PostsController.details);
router.delete('/posts/:id', PostsController.delete);
router.put('/posts/:id', PostsController.update);

// comment routes
router.get('/comments', CommentsController.list);
router.post('/comments', CommentsController.create);
router.get('/comments/:id', CommentsController.details);
router.delete('/comments/:id', CommentsController.delete);
router.put('/comments/:id', CommentsController.update);



export default router;